package main;

/*
 * NAME: Grant Sorbo
 * PARTNER: Juan Pablo Castano
 * CSC 242, SPRING 2016
 * PROJECT 3: UNCERTAIN INFERENCE
 * DATE LAST UPDATED: April 15, 2016
 * EMAIL: gsorbo@u.rochester.edu
 * FILENAME: ExactInference.java
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bn.core.Assignment;
import bn.core.BayesianNetwork;
import bn.core.Distribution;
import bn.core.Domain;
import bn.core.RandomVariable;
import bn.parser.BIFParser;
import bn.parser.XMLBIFParser;

public class ExactInference {

	public Distribution enumerationAsk(RandomVariable X, Assignment e, BayesianNetwork bn) {
		Distribution Q = new Distribution(X);
		Domain domain = X.getDomain();
		
		/* For each object in X's domain, (mostly T/F in our example cases),
		 * enumerate through all the BN RandVars according to an Assignment with that RV set to the specific Object
		 */
		for (int i = 0; i < domain.size(); i++) {
			Object x = domain.get(i);
			Assignment exi = e.copy();
			exi.set(X, x);
			Q.put(x, enumerateAll(bn, bn.getVariableListTopologicallySorted(), exi));
		}
		Q.normalize();
		return Q;
	}
	
	protected double enumerateAll(BayesianNetwork bn, List<RandomVariable> vars, Assignment e) {
	
		// If there are no variables left, return 1
		if(vars.isEmpty()) {
			return 1.0;
		}
		
		/* Pop next RV off list, check if it is already in the assignment
		 * If it is, return its probability according to the Assignment * recursion of the list minus itself
		 */
		RandomVariable Y = vars.get(0);
		if(e.containsKey(Y)) {
			return bn.getProb(Y, e) * enumerateAll(bn, vars.subList(1, vars.size()), e);
		}
		
		// else:
		double sum = 0.0;

		// sum of all possible assignments to that RV * same recursion as above
		for(Object y : Y.getDomain()) {
			Assignment ey = e.copy();
			ey.set(Y, y);

			sum = sum + bn.getProb(Y, ey) * enumerateAll(bn, vars.subList(1, vars.size()), ey);
		}
		return sum;
	}
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		BayesianNetwork bn = new BayesianNetwork();
		ExactInference test = new ExactInference();

		File file = new File(args[0]);
		
		// Use different parser depending on file extension
		if (file.getName().contains(".xml")) {
			System.out.println("xml");
			XMLBIFParser parser = new XMLBIFParser();
			bn = parser.readNetworkFromFile(file.getPath());
		}
		else if(file.getName().contains(".bif")) {
			System.out.println("bif");
			BIFParser parser = new BIFParser(new FileInputStream(file));
			bn = parser.parseNetwork();
		}
		else {
			System.out.println("Incompatible File Type, Please Restart.");
		}
			
		// Command line variable arguments
		RandomVariable X = bn.getVariableByName(args[1]);
		Assignment e = new Assignment();
		
		// for given values, loop through to add all to the Assignment
		for(int i=2; i<args.length; i+=2) {
			e.put(bn.getVariableByName(args[i]), args[i+1]);
			System.out.println("setting " + args[i] + " to " + args[i+1]);
		}
		
		// do the stuff
		Distribution dist = test.enumerationAsk(X, e, bn);
		System.out.println("|------Solving Inference------|");
		System.out.println(X.getName() + ": " + dist.toString());
	}
}

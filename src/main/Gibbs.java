package main;

/*
 * NAME: Grant Sorbo
 * PARTNER: Juan Pablo Castano
 * CSC 242, SPRING 2016
 * PROJECT 3: UNCERTAIN INFERENCE
 * DATE LAST UPDATED: April 15, 2016
 * EMAIL: gsorbo@u.rochester.edu
 * FILENAME: Gibbs.java
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bn.core.Assignment;
import bn.core.BayesianNetwork;
import bn.core.Distribution;
import bn.core.RandomVariable;
import bn.parser.BIFParser;
import bn.parser.XMLBIFParser;

public class Gibbs {
		
	public Distribution gibbsAsk(RandomVariable Q, Assignment e, BayesianNetwork bn, int max) throws IOException{
		Random rand = new Random();
		int[] vectorCounts = new int[Q.getDomain().size()];
		ArrayList<RandomVariable> nonEv = new ArrayList<RandomVariable>();
		
		for (RandomVariable rv : bn.getVariableList()){
			if (!e.containsKey(rv)){
				nonEv.add(rv);
				double val = rand.nextDouble();
				Object obj = rv.getRandomValueInit(val);
				e.put(rv, obj);
			} 
		}
				
		for (int i = 0; i < max; i++){
			for (RandomVariable z : nonEv){
				Distribution dist = getProbMB(bn,z,e);
				e.set(z, z.getRandomValue(dist));
				vectorCounts[Q.getDomain().indexOf(e.get(Q))] += 1;
			}
		}
		
		Distribution N = new Distribution();
		for (int i = 0; i<vectorCounts.length;i++){
			N.put(Q.getDomain().get(i), vectorCounts[i]);
			//System.out.println(vectorCounts[i]);
		}
		N.normalize();
		return N;
	}
	
	public Distribution getProbMB(BayesianNetwork bn, RandomVariable zi, Assignment e){
		Distribution newDist = new Distribution();
		Set<RandomVariable> children = bn.getChildren(zi);
		double prob;
		
		for (Object obj : zi.getDomain()){
			e.set(zi, obj);
			prob = bn.getProb(zi, e);
			for (RandomVariable child : children){
				prob = prob*bn.getProb(child, e);
			}
			
			newDist.put(obj, prob);
		}
		
		newDist.normalize();
		return newDist;
	}
	
	private BayesianNetwork getBnFromFile(String fileName) throws IOException, ParserConfigurationException, SAXException{
		XMLBIFParser xmlparser = new XMLBIFParser();
		FileInputStream stream = new FileInputStream(new File(fileName));
		BIFParser bifparser = new BIFParser(stream);
		
		if (fileName.endsWith("xml")||fileName.endsWith("bif")){
			if (fileName.endsWith("xml")){
				return xmlparser.readNetworkFromFile(fileName);
			} else {
				return bifparser.parseNetwork();
			}
		} else {
			throw new FileNotFoundException();
		}
	}	
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException{
		Gibbs newG = new Gibbs();
		int N = Integer.parseInt(args[0]);
		BayesianNetwork bn = newG.getBnFromFile(args[1]);
		RandomVariable X = bn.getVariableByName(args[2]);
		Assignment e = new Assignment();
		
		for(int i=3; i<args.length; i+=2) {
			e.put(bn.getVariableByName(args[i]), args[i+1]);
		}
		
		Distribution dist = newG.gibbsAsk(X, e, bn, N);
		System.out.println("|------Solving Inference------|");
		System.out.println(X.getName() + ": " + dist.toString());
		
	}
}

package main;

/*
 * NAME: Grant Sorbo
 * PARTNER: Juan Pablo Castano
 * CSC 242, SPRING 2016
 * PROJECT 3: UNCERTAIN INFERENCE
 * DATE LAST UPDATED: April 15, 2016
 * EMAIL: gsorbo@u.rochester.edu
 * FILENAME: LikelihoodWeighting.java
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bn.core.Assignment;
import bn.core.BayesianNetwork;
import bn.core.Distribution;
import bn.core.Domain;
import bn.core.RandomVariable;
import bn.parser.BIFParser;
import bn.parser.XMLBIFParser;

public class LikelihoodWeighting {

	public Distribution likelihoodWeighting(RandomVariable X, Assignment e, BayesianNetwork bn, int samples) {
		Domain domain = X.getDomain();
		double[] W = new double[domain.size()];
		
		for(int j=0; j<=samples; j++) {
			WeightedAssignment wa = weightedSample(bn, e); // call the weightedSample using BN and Assignment e
			W[domain.indexOf(wa.assignment.get(X))] += wa.getWeight();
		}
		
		// Turn array of doubles into distribution
		Distribution dist = new Distribution(X);		
		for(int i=0; i<W.length; i++) {
			dist.put(domain.get(i), W[i]);
		}
				
		dist.normalize();
		return dist;
	}
	
	protected WeightedAssignment weightedSample(BayesianNetwork bn, Assignment e) {
		WeightedAssignment wa = new WeightedAssignment();
		wa.setWeight(1.0); // initial weight of 1.0
		wa.assignment = e.copy();
		
		for(RandomVariable n : bn.getVariableListTopologicallySorted()) { // iterate through RVs in BN
			if(e.containsKey(n)) { // if the Assignment contains the RV
				wa.setWeight(wa.getWeight() * bn.getProb(n, wa.assignment)); // multiply current weight by probability of that RV
			}
			else {
				wa.assignment.set(n, randomSample(bn, n, wa.assignment)); // otherwise, assign that RV a random sample
			}
		}
		return wa;
	}
	
	protected Object randomSample(BayesianNetwork bn, RandomVariable n, Assignment a) {
		/* Basically the same randomSample method as RejectionSampling, with random double and an increasing sum
		 * to decide which object to choose as a ranodm sample to assign to the RV
		 */
		
		Random rand = new Random();
		double randDouble = rand.nextDouble();
		double sum = 0.0;
				
		for(Object o : n.getDomain()) {
			a.put(n, o);
			sum += bn.getProb(n, a);
			if(sum > randDouble) {
				return o;
			}
		}
		
		return null;
	}
	
	/* Custom returnable for weighted-sample. Just a dummy class that holds an Assignment and a double "weight" for use 
	 * when returning "two objects" like in the pseudo-code (AIMA Fig. 14.15)
	 */
	protected class WeightedAssignment {
		Assignment assignment;
		double weight;
		
		protected WeightedAssignment() {
			super();
			weight = 0.0;
		}
		
		protected void setWeight(double weight) {
			this.weight = weight;
		}
		
		protected double getWeight() {
			return weight;
		}
	}
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {	
		// See ExactInference for notes on choosing what to do for different file formats
		
		BayesianNetwork bn = new BayesianNetwork();
		LikelihoodWeighting test = new LikelihoodWeighting();

		File file = new File(args[1]);
		
		if (file.getName().contains(".xml")) {
			System.out.println("xml");
			XMLBIFParser parser = new XMLBIFParser();
			bn = parser.readNetworkFromFile(file.getPath());
		}
		else if(file.getName().contains(".bif")) {
			System.out.println("bif");
			BIFParser parser = new BIFParser(new FileInputStream(file));
			bn = parser.parseNetwork();
		}
		else {
			System.out.println("Incompatible File Type, Please Restart.");
		}
		
		RandomVariable X = bn.getVariableByName(args[2]);
		Assignment e = new Assignment();
		
		for(int i=3; i<args.length; i+=2) {
			e.put(bn.getVariableByName(args[i]), args[i+1]);
			System.out.println("setting " + args[i] + " to " + args[i+1]);
		}
		
		Distribution dist = test.likelihoodWeighting(X, e, bn, Integer.parseInt(args[0]));
		System.out.println(dist);
	}
}

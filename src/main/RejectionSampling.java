package main;

/*
 * NAME: Grant Sorbo
 * PARTNER: Juan Pablo Castano
 * CSC 242, SPRING 2016
 * PROJECT 3: UNCERTAIN INFERENCE
 * DATE LAST UPDATED: April 15, 2016
 * EMAIL: gsorbo@u.rochester.edu
 * FILENAME: RejectionSampling.java
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Random;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bn.core.Assignment;
import bn.core.BayesianNetwork;
import bn.core.Distribution;
import bn.core.Domain;
import bn.core.RandomVariable;
import bn.parser.BIFParser;
import bn.parser.XMLBIFParser;

public class RejectionSampling {

	Random rand = new Random();
	
	public Distribution rejectionSampling(RandomVariable X, Assignment e, BayesianNetwork bn, int samples) {
		Domain domain = X.getDomain();
		double[] N = new double[domain.size()];

		for(int j=1; j<=samples; j++) {
			Assignment x = priorSample(bn);
			
			// If the two Assignments are consistent, iterate the count for that object ("1 vote")
			if(isConsistent(e, x)) {
				N[domain.indexOf(x.get(X))] += 1.0;
			}
			
		}
		
		// Turn array of doubles into distribution
		Distribution dist = new Distribution(X);
	
		for(int i=0; i<N.length; i++) {
			dist.put(domain.get(i), N[i]);
		}
		
		dist.normalize(); // normalize (so that it sums to 1.0)
		return dist;
	}
	
	protected Assignment priorSample(BayesianNetwork bn) {
		Assignment x = new Assignment();
		
		// For each RV in the BN, put a random sample into the Assignment based on its probability (as denoted in BN)
		for(RandomVariable n : bn.getVariableListTopologicallySorted()) {
			x.put(n, randomSample(bn, n, x));
		}
		
		return x;
	}
	
	protected Object randomSample(BayesianNetwork bn, RandomVariable n, Assignment a) {
		Random rand = new Random();
		double randDouble = rand.nextDouble(); // generate a random double, between 0.0 and 1.0
		double sum = 0.0; // initialize "sum" which will increase as we iterate through the probs of the RV's domain
		
		for(Object o : n.getDomain()) {
			a.put(n, o); // assign o to the RV n in Assignment a
			sum += bn.getProb(n, a); // sum increases by amount of probability in current distribution
			if(sum > randDouble) {
				/* If the random double is below the current sum,
				 * we will assume that the double has "chosen" that 
				 * object as the random sample, so return o
				 */
				return o;
			}
		}
		return null;
	}
	
	private boolean isConsistent(Assignment a, Assignment b) {
		for(RandomVariable X : a.keySet()) {
			
			/* If any value of a RandomVariable in the subset (b) of the 
			 * Assignment (a) is not found in the full Assignment, then 
			 * it is not consistent, return false
			 */
			if(!(a.get(X).equals(b.get(X)))) {
				return false;
			}
		}
		// Otherwise, it is consistent
		return true;
	}
	
	
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		// See ExactInference.java for notes on deciding what to do with different file formats
		
		BayesianNetwork bn = new BayesianNetwork();
		RejectionSampling test = new RejectionSampling();

		File file = new File(args[1]);
		
		if (file.getName().contains(".xml")) {
			System.out.println("xml");
			XMLBIFParser parser = new XMLBIFParser();
			bn = parser.readNetworkFromFile(file.getPath());
		}
		else if(file.getName().contains(".bif")) {
			System.out.println("bif");
			BIFParser parser = new BIFParser(new FileInputStream(file));
			bn = parser.parseNetwork();
		}
		else {
			System.out.println("Incompatible File Type, Please Restart.");
		}
		
		RandomVariable X = bn.getVariableByName(args[2]);
		Assignment e = new Assignment();
		
		for(int i=3; i<args.length; i+=2) {
			e.put(bn.getVariableByName(args[i]), args[i+1]);
			System.out.println("setting " + args[i] + " to " + args[i+1]);
		}
		
		Distribution dist = test.rejectionSampling(X, e, bn, Integer.parseInt(args[0]));
		System.out.println(dist);
	}
}
